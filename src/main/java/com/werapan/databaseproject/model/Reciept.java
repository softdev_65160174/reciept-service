/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Reciept {
     private int id;
     private Date createdDate;
     private float total;
     private float cash;
     private int totalQty;
     private int userID;
     private int recieptID;
     private int customerID;
     // make User and Customer to obj of Reciept   
     private User user;
     private Customer customer;
     //Reciept Deatial
     private ArrayList<RecieptDetail> reciptDetails = new ArrayList();

    public Reciept(int id, Date createdDate, float total, float cash, int totalQty,int userID, int recieptID, int customerID) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userID = userID;
        this.recieptID = recieptID;
        this.customerID = customerID;
    }
    
    public Reciept(Date createdDate, float total, float cash, int totalQty,int userID, int recieptID, int customerID) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userID = userID;
        this.recieptID = recieptID;
        this.customerID = customerID;
    }
    
    public Reciept( float total, float cash, int totalQty,int userID, int recieptID, int customerID) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userID = userID;
        this.recieptID = recieptID;
        this.customerID = customerID;
    }
    
    public Reciept(float cash, int userID, int recieptID, int customerID) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = cash;
        this.totalQty = 0;
        this.userID = userID;
        this.recieptID = recieptID;
        this.customerID = customerID;
    }
    
    public Reciept() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.totalQty = 0;
        this.userID = 0;
        this.recieptID = 0;
        this.customerID = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getRecieptID() {
        return recieptID;
    }

    public void setRecieptID(int recieptID) {
        this.recieptID = recieptID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userID = user.getId();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerID = customer.getId();
    }

    public ArrayList<RecieptDetail> getReciptDetails() {
        return reciptDetails;
    }

    public void setReciptDetails(ArrayList reciptDetails) {
        this.reciptDetails = reciptDetails;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", totalQty=" + totalQty + ", userID=" + userID + ", recieptID=" + recieptID + ", customerID=" + customerID + ", user=" + user + ", customer=" + customer + ", reciptDetails=" + reciptDetails + '}';
    }
    
    public void addRecieptDetail(RecieptDetail recieptDetail){
        reciptDetails.add(recieptDetail);
        calculateTotal();
    }
    
    public void delRecieptDetail(RecieptDetail recieptDetail){
        reciptDetails.remove(recieptDetail);
        calculateTotal();
    }
    
    private void calculateTotal(){
        int totalQty = 0;
        float total = 0.0f;
        for(RecieptDetail rd : reciptDetails){
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        
        this.totalQty = totalQty;
        this.total = total;
    }
   

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreatedDate(rs.getTimestamp("created_date"));
            reciept.setTotal(rs.getFloat("total"));
            reciept.setCash(rs.getFloat("cash"));
            reciept.setTotalQty(rs.getInt("total_qty"));
            reciept.setUserID(rs.getInt("user_id"));
            reciept.setCustomerID(rs.getInt("customer_id"));
            
            // Population User & Customer in Reciept mean make User and Customer to obj of Reciept
            CustomerDao customerDao = new CustomerDao();
            UserDao userDao = new UserDao();
            Customer customer = customerDao.get(reciept.getCustomerID());
            User user = userDao.get(reciept.getUserID());
            reciept.setCustomer(customer);
            reciept.setUser(user);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }

}
